let humanFrendly = "";

const coin = {
    state: 0,
    flip: function () {
        this.state = Math.round(Math.random());
        return this.state;
    },

    toString: function () {
        if (this.state === 0) {
            humanFrendly = "Heads";
        }
        else if (this.state === 1) {
            humanFrendly = "Tails";
        }
        return humanFrendly;
    },
    toHTML: function () {

        const image = document.createElement('img');
        image.width = 80;
        image.height = 80;
        if (this.state === 0) {
            image.src = "images/head.png";
            document.body.appendChild(image);
            return image;
        }
        else if (this.state === 1) {
            image.src = "images/tails.png";
            document.body.appendChild(image);
            return image;
        }
    }
};

function display20Flips() {
    const results = [];
    for (i = 0; i < 20; i++) {
        coin.flip()
        coin.toString()
        results[i] = " " + humanFrendly;

        if (i === 19) {
            let resultsInHTML = document.createElement('div');
            resultsInHTML.textContent = results;
            document.body.appendChild(resultsInHTML);
        }
    }
}

display20Flips()

function display20Images() {
    const results = [];
    for (i = 0; i < 20; i++) {
        coin.flip()
        coin.toString()
        results[i] = " " + humanFrendly;
        coin.toHTML()

        if (i === 19) {
            let resultsInHTML = document.createElement('div');
            resultsInHTML.textContent = results;
            document.body.appendChild(resultsInHTML);
        }
    }
}

display20Images()